import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AmplifyService } from '../services';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  constructor(private amplifyService: AmplifyService) {}
  public BASE_URL: any;
  public BASE_URL_SOCKET: any;
  public token: any;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    const token = ""// TODO this.amplifyService.getToken();

    if (token) {
      headersConfig['Authorization'] = `Token ${token}`;
    }

    const request = req.clone({ setHeaders: headersConfig });
    //return next.handle(request);
    this.BASE_URL = localStorage.getItem('BASE_URL');
    for (let i = 0, len = localStorage.length; i < len; ++i) {
      if (localStorage.key(i).includes('idToken')) {
        this.token = localStorage.getItem(localStorage.key(i));
      }
    }
    if (this.token != null) {
      if (req.url.substr(0, 3) === '../') {
        const authReq = req.clone({
          headers: req.headers.set('Authorization', 'Bearer ' + this.token),
        });
        return next.handle(authReq).pipe();
      } else {
        const authReq = req.clone(
          {
            headers: req.headers.set('Authorization', 'Bearer ' + this.token),
            url: this.BASE_URL + req.url

          });
        return next.handle(authReq).pipe();
      }
    } else {
      if (req.url.substr(0, 3) === '../') {
        const authReq = req.clone(
          {}
        );
        return next.handle(authReq).pipe();
      } else {
        const authReq = req.clone(
          {
            url: this.BASE_URL + req.url
          }
        );
        return next.handle(authReq).pipe();
      }
    }
  }
  }


