import {Injectable} from '@angular/core';
import {Amplify, Auth} from 'aws-amplify';
import {sha256} from 'js-sha256';
import Storage from '@aws-amplify/storage';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';

export interface AuthState {
  isLoggedIn: boolean;
  username: string | null;
  email: string | null;
  id: string | null;
}

const initialAuthState = {
  isLoggedIn: false,
  username: null,
  email: null,
  id: null
};

@Injectable({
  providedIn: 'root'
})

export class AmplifyService {

  config: any;
  private readonly authState = new BehaviorSubject<AuthState>(
    initialAuthState
  );

  /** AuthState as an Observable */
  readonly auth$ = this.authState.asObservable();

  /** Observe the isLoggedIn slice of the auth state */
  readonly isLoggedIn$ = this.auth$.pipe(map(state => state.isLoggedIn));

  constructor() {
   // console.log('const');
    // Get the user on creation of this service
    Auth.currentAuthenticatedUser({bypassCache: false}).then(
      (user: any) => {
        this.setUser(user);
      },
      err => {
        this.authState.next(initialAuthState);
      }
    );
  }

  /** signin */
  public signIn(email: string, password: string): Observable<any> {
    return from(Auth.signIn(email, password)).pipe(
      tap((data) => {
        //console.log("2: ", data);
        this.setUser(data);
        return data;
      })
    );
  }

  /** signup */
  public signUp(username: string, password: string, email: string): Observable<any> {
    return from(Auth.signUp({username, password, attributes: {email}}));
  }

  /** signout */
  public signOut() {
    from(Auth.signOut())
      .subscribe(
        result => {
          this.authState.next(initialAuthState);
        },
        error => console.log(error)
      );
  }

  sendForgotPassword(username: any) {
    return Auth.forgotPassword(username);
  }

  setUser(user: any) {
    //!user = user not defined
    if (!user) {
      return;
    }
    const {
      attributes: {sub: id, email},
      username

    } = user;

    this.authState.next({isLoggedIn: true, id, username, email});
  }

  updatePassword(oldPassword: string, password: string): Promise<any> {
    return new Promise<any>((async (resolve, reject) => {
      this.getCurrentUser().then((user) => {
        Auth.changePassword(user, oldPassword, password).then((result) => {
          resolve(result);
        }).catch((err) => {
          reject(err);
        });
      }).catch((err) => {
        reject(err);
      });
    }));
  }

  changePassword(username: any, code: any, newPassword: any) {
    return Auth.forgotPasswordSubmit(username, code, newPassword);
  }

  getCurrentUser() {
    return Auth.currentAuthenticatedUser();
  }

  getUserInfo() {
    return Auth.currentUserInfo();
  }

  upload(file: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      fetch('../../../config/config.json').then(res => {
        // tslint:disable-next-line:no-shadowed-variable
        res.json().then((res) => {
          Auth.configure(res.amplify.Auth);
          this.config = res.amplify;
          const path = new Date().getUTCFullYear() + '/' + Number(new Date().getUTCMonth() + 1) + '/' + new Date().getUTCDate() + '/';
          this.config.Storage.customPrefix.public += path;
          Amplify.configure(this.config);
        })
          .then(() => {
            this.getCurrentUser()
              .then((user) => {
                try {
                  let extension = '';
                  const hash = sha256.hmac(new Date(Date.now()).toString(), file.name);
                  JSON.parse(JSON.stringify(file)).hash = hash;

                  // if type different -1 found, the extension image display dans s3
                  // tslint:disable-next-line:triple-equals
                  if (file.type.indexOf('image/') != -1) {
                    extension = file.type.substr(6);
                    Storage.put(hash + '.' + extension, file, this.config);
                  }

                  // if type different -1 found, the extension  pdf display dans s3
                  // tslint:disable-next-line:triple-equals
                  if (file.type.indexOf('application/pdf') != -1) {
                    Storage.put(hash + '.pdf', file, this.config);
                  }
                  resolve({path: this.config.Storage.customPrefix.public, fileHash: hash});
                } catch (err) {
                  reject(err);
                }
              })
              .catch((err) => {
                reject(err);
              });
          });
      });
    });
  }

  uploadFromVariable(file: any) {
    return new Promise<any>((resolve, reject) => {
      fetch('../../../config/config.json').then((rep) => {
        const path = new Date().getUTCFullYear() + '/' + Number(new Date().getUTCMonth() + 1) + '/' + new Date().getUTCDate() + '/';
        let conf;
        rep.json().then((res) => {
          conf = res.amplify;
          conf.Storage.customPrefix.public += path;
          Amplify.configure(conf);
        }).then(() => {
          const hash = sha256.hmac(new Date(Date.now()).toString(), 'file.pdf');
          Storage.put(hash + '.pdf', file, conf);
          resolve({path: conf.Storage.customPrefix.public, fileHash: hash});
        });
      }).catch((err) => {
        reject(err);
      });
    });
  }

  async deleteFile(fileName: string, pathFile: string, level: string): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      fetch('../../../config/config.json').then(res => {
        // tslint:disable-next-line:no-shadowed-variable
        res.json().then((res) => {
          Auth.configure(res.amplify.Auth);
          this.config = res.amplify;
          Amplify.configure(this.config);
        }).then(() => {
          this.getCurrentUser()
            .then(async (user) => {
              await Storage.remove(pathFile + fileName, {level: level.toLowerCase()}).then((data) => {
                resolve(data);
              }).catch((err) => {
                console.log(err);
                reject(err);
              });
            })
            .catch((err) => {
              console.log(err);
              reject(err);
            })
            .catch((e) => {
              console.log(e);
              reject(e);
            });
        });
      });
    });
  }

  getUser() {
    return Auth.currentAuthenticatedUser()
      .then(userData => userData)
      .catch(() => console.log('Not signed in'));
  }

  async getFile(fileName: string, pathFile: string, level: string): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      fetch('../../../config/config.json').then(res => {
        // tslint:disable-next-line:no-shadowed-variable
        res.json().then((res) => {
          Auth.configure(res.amplify.Auth);
          this.config = res.amplify;
          Amplify.configure(this.config);
        }).then(() => {
          console.warn('pathFile', pathFile, 'fileName', fileName);
          Storage.get(pathFile + fileName, {level: level.toLowerCase()}).then((data) => {
            resolve(data);
          }).catch((err) => {
            console.log(err);
            reject(err);
          });
        })
          .catch((e) => {
            console.log(e);
            reject(e);
          });
      });
    });
  }
}
