import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, BehaviorSubject, ReplaySubject} from 'rxjs';

import {ApiService} from './api.service';
import {User} from '../models';
import {map, distinctUntilChanged, tap} from 'rxjs/operators';
import {AmplifyService} from './amplify.service';
import {error} from 'protractor';
import {get} from 'http';
import {HttpParams} from '@angular/common/http';
import {UserapiService} from './userapi.service';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class UserService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();
  private user: User;


  constructor(
    private amplifyService: AmplifyService,
    private apiService: ApiService,
    private userapiservice : UserapiService,
    private http: HttpClient,
    private router: Router
  ) {
  }

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.

  populate() {
    // If JWT detected, attempt to get & store user's info
    this.amplifyService.getCurrentUser().then(data => {
      console.log(data);
      this.setAuth(data);
    }).catch(error => {
      console.log(error);
      this.purgeAuth();
    });


  }

  setAuth(user: any) {
    // Set current user data into observable
    this.currentUserSubject.next(user);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.amplifyService.signOut();
    // Set current user to an empty object

    this.currentUserSubject.next({} as User);

    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(type, credentials): Observable<User> {
    const route = (type === 'login') ? '/login' : '';
    if (route === '/login') {
      //Subscribe() est une méthode dans Angular qui connecte le observer aux événements observable .
      // Chaque fois qu'une modification est apportée à ces observables,
      // un code est exécuté et observe les résultats ou les modifications à l'aide de la méthode subscribe
      /*******************************amplifyService.signIn with Subscribe******************************/
        this.amplifyService.signIn(credentials.email, credentials.password).subscribe(result => {
          console.log("hello from sign in")
        this.setAuth(this.amplifyService.getCurrentUser().then(userData => userData)
          .catch(() => console.log('Not signed in')));
        /*************************SORT INFORMATION COGNITO**********************************************/
        this.user = new User(result.username.trim().replace(/\s/g, ''), result.attributes.email.trim().replace(/\s/g, ''));
        this.user.email_verified = result.attributes.email_verified;
        this.user.sub = result.attributes.sub;
        this.user.clientId = result.pool.clientId;
        this.user.userPoolId = result.pool.userPoolId;
        console.log("user when log in", this.user)
        //this.apiService.post('/users/', new User(credentials.username, credentials.password)).subscribe(result => {console.log("4")});
        /*******************add USER IN BACK WITH MY SERVICE AJOUTER****************************************/
        this.userapiservice.ajoutUser(this.user).then(async (user: User) => {
          console.log("this.user back", this.user)
          localStorage.setItem("myId", user._id)
          this.user._id = user._id;
        });

      })
      return this.currentUser;
    } else {
      this.amplifyService.signUp(credentials.username, credentials.password, credentials.email);
    }
  }

  setUser(user: User) {
    localStorage.setItem('userObject', JSON.stringify(user));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }


  // Update the user on the server (email, pass, etc)
  update(user): Observable<User> {
    return this.apiService
      .put('/users/', {user})
      .pipe(map(data => {
        // Update the currentUser observable
        this.currentUserSubject.next(data.user);
        return data.user;
      }));
  }

}
