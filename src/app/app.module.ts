import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import {
  FooterComponent,
  HeaderComponent,
  SharedModule
} from './shared';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import {AmplifyService, AuthGuard} from "./core";
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from "@angular/forms";
import {MegaMenuModule} from 'primeng/megamenu';
import {ButtonModule} from 'primeng/button';
import {MenubarModule} from 'primeng/menubar';


@NgModule({
  declarations: [AppComponent, FooterComponent, HeaderComponent],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        CoreModule.forRoot(),
        SharedModule,
        AuthModule,
        AppRoutingModule,
        MegaMenuModule,
        ButtonModule,
        MenubarModule
    ],

  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {

constructor(auth: AmplifyService) {
  console.log('starting AppModule');
}
}
