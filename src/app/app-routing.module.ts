import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

const routes: Routes = [

  { path: 'templates', loadChildren: () => import('./templates/templates.module').then(m => m.TemplatesModule) },

];

@NgModule({
  imports: [HttpClientModule,
    RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    relativeLinkResolution: 'legacy'
})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
